#include <iostream>
#include <map>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "stb_image.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include<learnopengl/shader.h>
#include<learnopengl/camera.h>
#include<learnopengl/model.h>


#include "Actions.h"

#include <irrklang/irrKlang.h>
using namespace irrklang;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void process_input(GLFWwindow* window);
unsigned int load_texture(const char* path);
unsigned int load_cubemap(vector<std::string> faces);
void process_piano_keys_movements(GLFWwindow* window, float deltaTime);
void click_flashlight();
void draw_objects(Shader& shader, const glm::mat4 base_pos);
void draw_keys(Shader& shader, const glm::mat4 starting_pos);
void draw_lamps(Shader& lighting_shader, Shader& lamp_shader, const glm::mat4 projection, const glm::mat4 view, const glm::mat4 base_pos);

// settings
const unsigned int SCR_WIDTH = 1366;
const unsigned int SCR_HEIGHT = 720;

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

//Actions
Actions actions(1.0f);

bool flashlight_on = false;
bool flashlight_pressed = false;

std::map <std::string, Model*> model_map;

irrklang::ISoundEngine* engine = irrklang::createIrrKlangDevice();

glm::vec3 lamp_colors[] = {
		glm::vec3(0.8f, 0.0f, 0.0f),
		glm::vec3(0.0f, 0.8f, 0.0f),
		glm::vec3(0.0f, 0.0f, 0.8f),
};

bool movement = true; // when true move with WASD, when false play the piano

int main()
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetKeyCallback(window, key_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// build and compile shaders
	// -------------------------
	Shader lighting_shader("main.vs.glsl", "main.fs.glsl");
	Shader lamp_shader("lamp.vs.glsl", "lamp.fs.glsl");
	Shader skybox_shader("cube_map.vs.glsl", "cube_map.fs.glsl");

	vector<std::string> skybox_faces
	{
		"textures/skybox/right.jpg",
		"textures/skybox/left.jpg",
		"textures/skybox/up.jpg",
		"textures/skybox/bottom.jpg",
		"textures/skybox/forward.jpg",
		"textures/skybox/backward.jpg"
	};

	unsigned int cubemap_texture = load_cubemap(skybox_faces);

	float skybox_vertices[] = {
		-1.0f,  1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f, -1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f, -1.0f,  1.0f,
		-1.0f, -1.0f,  1.0f,

		-1.0f,  1.0f, -1.0f,
		1.0f,  1.0f, -1.0f,
		1.0f,  1.0f,  1.0f,
		1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f,  1.0f,
		-1.0f,  1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f,  1.0f,
		1.0f, -1.0f,  1.0f
	};

	unsigned int skyboxVAO, skyboxVBO;
	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skybox_vertices), &skybox_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	// load textures (we now use a utility function to keep the code more organized)
	// -----------------------------------------------------------------------------
	unsigned int diffuse_map = load_texture(((string)("textures/diffuse.png")).c_str());
	unsigned int specular_map = load_texture(((string)("textures/specular.png")).c_str());

	// shader configuration
	// --------------------
	lighting_shader.use();
	lighting_shader.setInt("material.diffuse", 0);
	lighting_shader.setInt("material.specular", 1);

	model_map.insert(std::make_pair("piano", new Model((string)"models/piano/Pianotex.obj")));
	model_map.insert(std::make_pair("key_white", new Model((string)"models/piano/white.obj")));
	model_map.insert(std::make_pair("key_black", new Model((string)"models/piano/black.obj")));
	model_map.insert(std::make_pair("paper", new Model((string)"models/piano/paper.obj")));
	model_map.insert(std::make_pair("piano_flap", new Model((string)"models/piano/flap.obj")));
	model_map.insert(std::make_pair("stick", new Model((string)"models/piano/stick.obj")));
	model_map.insert(std::make_pair("stage", new Model((string)"models/stage/stage2.obj")));
	model_map.insert(std::make_pair("lamp", new Model((string)"models/stage/lamp.obj")));
	model_map.insert(std::make_pair("lens", new Model((string)"models/stage/lens.obj")));

	skybox_shader.use();
	skybox_shader.setInt("skybox", 0);

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		process_input(window);
		actions.move_the_lamps(deltaTime);

		glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// be sure to activate shader when setting uniforms/drawing objects
		lighting_shader.use();
		lighting_shader.setVec3("viewPos", camera.Position);
		lighting_shader.setFloat("material.shininess", 32.0f);

		// directional light
		lighting_shader.setVec3("dirLight.direction", -2.3f, -3.0f, 5.3f);
		lighting_shader.setVec3("dirLight.ambient", 0.05f, 0.05f, 0.05f);
		lighting_shader.setVec3("dirLight.diffuse", 1.0f, 1.0f, 1.0f);
		lighting_shader.setVec3("dirLight.specular", 0.5f, 0.5f, 0.5f);

		// spotLight
		if (flashlight_on) {
			lighting_shader.setVec3("spotLight[0].ambient", 0.2f, 0.2f, 0.2f);
			lighting_shader.setVec3("spotLight[0].diffuse", 0.7f, 0.7f, 0.7f);
			lighting_shader.setVec3("spotLight[0].specular", 0.2f, 0.2f, 0.2f);
		}
		else {
			lighting_shader.setVec3("spotLight[0].ambient", 0.0f, 0.0f, 0.0f);
			lighting_shader.setVec3("spotLight[0].diffuse", 0.0f, 0.0f, 0.0f);
			lighting_shader.setVec3("spotLight[0].specular", 0.0f, 0.0f, 0.0f);
		}
		lighting_shader.setVec3("spotLight[0].position", camera.Position);
		lighting_shader.setVec3("spotLight[0].direction", camera.Front);
		lighting_shader.setFloat("spotLight[0].constant", 1.0f);
		lighting_shader.setFloat("spotLight[0].linear", 0.09);
		lighting_shader.setFloat("spotLight[0].quadratic", 0.032);
		lighting_shader.setFloat("spotLight[0].cutOff", glm::cos(glm::radians(12.5f)));
		lighting_shader.setFloat("spotLight[0].outerCutOff", glm::cos(glm::radians(15.0f)));

		// view/projection transformations
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
		glm::mat4 view = camera.GetViewMatrix();
		lighting_shader.setMat4("projection", projection);
		lighting_shader.setMat4("view", view);

		// world transformation
		glm::mat4 model = glm::mat4(1.0f);
		lighting_shader.setMat4("model", model);

		// bind diffuse map
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, diffuse_map);
		// bind specular map
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, specular_map);

		glm::mat4 starting_pos = glm::mat4(1.0f);
		starting_pos = glm::translate(starting_pos, glm::vec3(0.0f, -1.2f, 0.0f));

		draw_objects(lighting_shader, starting_pos);

		draw_lamps(lighting_shader, lamp_shader, projection, view, starting_pos);

		// draw skybox
		glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's content
		skybox_shader.use();
		view = glm::mat4(glm::mat3(camera.GetViewMatrix())); // remove translation from the view matrix
		skybox_shader.setMat4("view", view);
		skybox_shader.setMat4("projection", projection);

		// skybox cube
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap_texture);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glDepthFunc(GL_LESS); // set depth function back to default

		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	glDeleteVertexArrays(1, &skyboxVAO);
	glDeleteBuffers(1, &skyboxVBO);
	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}


void draw_objects(Shader& shader, const glm::mat4 starting_pos) {
	// Draw the piano
	glm::mat4 model = glm::scale(starting_pos, glm::vec3(0.8f)); // Scale down the piano
	shader.setMat4("model", model);
	shader.setFloat("material.shininess", 128.0f);
	model_map["piano"]->Draw(shader);

	draw_keys(shader, starting_pos);

	// Draw paper
	glm::mat4 paper_transform = glm::translate(starting_pos, glm::vec3(0.0f, 1.02f, 0.64f));
	paper_transform = glm::rotate(paper_transform, glm::radians(81.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	paper_transform = glm::scale(paper_transform, glm::vec3(0.18f, 0.01f, 0.16f));
	shader.setMat4("model", paper_transform);
	model_map["paper"]->Draw(shader);

	// Draw piano flap
	glm::mat4 flap_transform = glm::translate(starting_pos, glm::vec3(-0.786f, 0.91f, -0.928f));
	flap_transform = glm::rotate(flap_transform, glm::radians(actions.get_flop_angle()), glm::vec3(0.0f, 0.0f, 1.0f));
	flap_transform = glm::scale(flap_transform, glm::vec3(0.86f, 0.825f, 0.870f));
	shader.setMat4("model", flap_transform);
	model_map["piano_flap"]->Draw(shader);

	// Draw stick
	glm::mat4 stick_transform = glm::translate(starting_pos, glm::vec3(0.77f, 0.89f, 0.52f));
	stick_transform = glm::rotate(stick_transform, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	stick_transform = glm::rotate(stick_transform, glm::radians(actions.get_stick_angle()), glm::vec3(0.0f, 0.0f, 1.0f));
	stick_transform = glm::scale(stick_transform, glm::vec3(0.85f, 0.7f, 0.7f));
	shader.setMat4("model", stick_transform);
	model_map["stick"]->Draw(shader);

	// Draw stage
	glm::mat4 stage_transform = glm::translate(starting_pos, glm::vec3(0.0f, -1.476f, 0.0f));
	shader.setMat4("model", stage_transform);
	model_map["stage"]->Draw(shader);
}

void draw_keys(Shader& shader, const glm::mat4 starting_pos) {
	// Draw white keys
	glm::mat4 keys_pos = glm::translate(starting_pos, glm::vec3(-0.72f, 0.66f, 0.75f));
	glm::mat4 key_scale = glm::scale(glm::mat4(1.0f), glm::vec3(1.0f));
	for (unsigned int i = 0; i < 36; ++i) {
		glm::mat4 key_transform = glm::translate(keys_pos, glm::vec3(0.042f * i, 0.0f, 0.0f));
		key_transform = glm::rotate(key_transform, glm::radians(actions.get_piano_key_angle(i, true)), glm::vec3(1.0f, 0.0f, 0.0f));
		key_transform = key_scale * key_transform;
		shader.setMat4("model", key_transform);
		model_map["key_white"]->Draw(shader);
	}

	// Draw black keys
	glm::mat4 keys_pos_black = glm::translate(keys_pos, glm::vec3(0.02f, 0.0f, 0.0f));
	bool add_four = true;
	unsigned int when_blank = 2;
	for (unsigned int i = 0, black_key_number = 0; i < 35; ++i) {
		if (i == when_blank) {
			when_blank += add_four ? 4 : 3;
			add_four = !add_four;
			continue;
		}
		glm::mat4 key_transform = glm::translate(keys_pos_black, glm::vec3(0.042f * i, 0.0f, 0.0f));
		key_transform = glm::rotate(key_transform, glm::radians(actions.get_piano_key_angle(black_key_number++, false)), glm::vec3(1.0f, 0.0f, 0.0f));
		key_transform = key_scale * key_transform;
		shader.setMat4("model", key_transform);
		model_map["key_black"]->Draw(shader);
	}
}

void draw_lamps(Shader& lighting_shader, Shader& lamp_shader, const glm::mat4 projection, const glm::mat4 view, const glm::mat4 starting_pos) {
	glDisable(GL_CULL_FACE);

	glm::mat4 lamp_positions[3];
	Model* lamp_model = model_map.at("lamp");

	for (int i = 0; i < 3; ++i) {
		glm::vec3 light_move_dir = actions.get_light_direction_move(i);

		glm::mat4 model_matrix = glm::translate(starting_pos, glm::vec3(-8.0f + 6.5f * i, 9.6f, 5.47f));
		lamp_positions[i] = model_matrix;

		lighting_shader.setMat4("model", model_matrix);
		lamp_model->Draw(lighting_shader);

		std::string light_name = "spotLight[" + std::to_string(i + 1) + "].";
		glm::vec3 light_position(model_matrix[3][0], model_matrix[3][1], model_matrix[3][2]);
		glm::vec3 light_direction = glm::normalize(glm::vec3(
			starting_pos[3][0] - model_matrix[3][0] + light_move_dir.x,
			starting_pos[3][1] - model_matrix[3][1] + light_move_dir.y,
			starting_pos[3][2] - model_matrix[3][2] + light_move_dir.z
		));

		lighting_shader.setVec3(light_name + "position", light_position);
		lighting_shader.setVec3(light_name + "direction", light_direction);
		lighting_shader.setVec3(light_name + "ambient", lamp_colors[i]);
		lighting_shader.setVec3(light_name + "diffuse", lamp_colors[i]);
		lighting_shader.setVec3(light_name + "specular", lamp_colors[i]);
		lighting_shader.setFloat(light_name + "constant", 1.0f);
		lighting_shader.setFloat(light_name + "linear", 0.0009f);
		lighting_shader.setFloat(light_name + "quadratic", 0.00092f);
		lighting_shader.setFloat(light_name + "cutOff", glm::cos(glm::radians(12.5f)));
		lighting_shader.setFloat(light_name + "outerCutOff", glm::cos(glm::radians(15.0f)));
	}

	glEnable(GL_CULL_FACE);

	Model* lens_model = model_map.at("lens");
	lamp_shader.use();
	lamp_shader.setMat4("projection", projection);
	lamp_shader.setMat4("view", view);

	for (int i = 0; i < 3; ++i) {
		glm::mat4 lens_matrix = lamp_positions[i];
		lens_matrix = glm::translate(lens_matrix, glm::vec3(0.0f, -0.08f, -0.64f));
		lens_matrix = glm::rotate(lens_matrix, glm::radians(-129.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		lens_matrix = glm::scale(lens_matrix, glm::vec3(0.37f));

		lamp_shader.setMat4("model", lens_matrix);
		lens_model->Draw(lamp_shader);
	}
}


// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void process_input(GLFWwindow* window)
{

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS && movement)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS && movement)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS && movement)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS && movement)
		camera.ProcessKeyboard(RIGHT, deltaTime);

	int flashlight_key = GLFW_KEY_TAB;
	if (glfwGetKey(window, flashlight_key) == GLFW_PRESS) {
		click_flashlight();
	}
	if (glfwGetKey(window, flashlight_key) == GLFW_RELEASE) {
		flashlight_pressed = false;
	}

	if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS) {
		actions.ProcessKeyboard(PIANO_OPEN, deltaTime);
	}
	if (glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS) {
		actions.ProcessKeyboard(PIANO_CLOSE, deltaTime);
	}

	process_piano_keys_movements(window, deltaTime);
}

void process_piano_keys_movements(GLFWwindow* window, float deltaTime) {
	// White keys input
	// -----------------
	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 14, true);
	}
	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 14, true);
	}

	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 15, true);
	}
	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_RELEASE && !movement) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 15, true);
	}

	if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 16, true);
	}
	if (glfwGetKey(window, GLFW_KEY_C) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 16, true);
	}

	if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 17, true);
	}
	if (glfwGetKey(window, GLFW_KEY_V) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 17, true);
	}

	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 18, true);
	}
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 18, true);
	}

	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 19, true);
	}
	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 19, true);
	}

	if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 20, true);
	}
	if (glfwGetKey(window, GLFW_KEY_M) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 20, true);
	}

	if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 21, true);
	}
	if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 21, true);
	}

	if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 22, true);
	}
	if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 22, true);
	}

	if (glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 23, true);
	}
	if (glfwGetKey(window, GLFW_KEY_SLASH) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 23, true);
	}

	if (glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 24, true);
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 24, true);
	}

	// black keys input
	// ----------------
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 10, false);
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 10, false);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 11, false);
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 11, false);
	}
	if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 12, false);
	}
	if (glfwGetKey(window, GLFW_KEY_G) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 12, false);
	}
	if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 13, false);
	}
	if (glfwGetKey(window, GLFW_KEY_H) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 13, false);
	}
	if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 14, false);
	}
	if (glfwGetKey(window, GLFW_KEY_J) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 14, false);
	}
	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 15, false);
	}
	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 15, false);
	}
	if (glfwGetKey(window, GLFW_KEY_SEMICOLON) == GLFW_PRESS && !movement) {
		actions.ProcessKeyboard(PUSH_KEY, deltaTime, 16, false);
	}
	if (glfwGetKey(window, GLFW_KEY_SEMICOLON) == GLFW_RELEASE) {
		actions.ProcessKeyboard(RELEASE_KEY, deltaTime, 16, false);
	}
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){

	if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
	{ 
		// switch scence mode
		movement = !movement;
	}

	// WHITE KEYS INPUT
	// ----------------
	else if (key == GLFW_KEY_Z && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/C3.mp3", false);
	}else if (key == GLFW_KEY_X && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/D3.mp3", false);
	}else if (key == GLFW_KEY_C && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/E3.mp3", false);
	}else if (key == GLFW_KEY_V && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/F3.mp3", false);
	}else if (key == GLFW_KEY_B && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/G3.mp3", false);
	}else if (key == GLFW_KEY_N && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/A3.mp3", false);
	}else if (key == GLFW_KEY_M && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/B3.mp3", false);
	}
	else if (key == GLFW_KEY_COMMA && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/C4.mp3", false);	
	}else if (key == GLFW_KEY_PERIOD && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/D4.mp3", false);
	}else if (key == GLFW_KEY_SLASH && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/E4.mp3", false);
	}else if (key == GLFW_KEY_RIGHT_SHIFT && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/F4.mp3", false);
	}

	// BLACK KEYS INPUT
	// ----------------
	else if (key == GLFW_KEY_S && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/Db3.mp3", false);
	}else if (key == GLFW_KEY_D && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/Eb3.mp3", false);
	}else if (key == GLFW_KEY_G && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/Gb3.mp3", false);
	}else if (key == GLFW_KEY_H && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/Ab3.mp3", false);
	}else if (key == GLFW_KEY_J && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/Bb4.mp3", false);
	}else if (key == GLFW_KEY_L && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/Db4.mp3", false);
	}else if (key == GLFW_KEY_SEMICOLON && action == GLFW_PRESS && !movement) {
		engine->play2D("music-notes/Eb4.mp3", false);
	}
}

void click_flashlight() {
	if (flashlight_pressed) return;
	flashlight_on = !flashlight_on;
	flashlight_pressed = true;
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned int load_texture(char const* path)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}

unsigned int load_cubemap(vector<std::string> faces)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
			);
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}