#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

enum key_action {
    PIANO_OPEN,
    PIANO_CLOSE,
    PUSH_KEY,
    RELEASE_KEY
};

const float PIANO_FLOP_OPEN_ANGLE_MAX = 31.0f;
const float PIANO_STICK_OPEN_ANGLE_MAX = 42.0f;
const float PIANO_KEY_PUSH_ANGLE_MAX = 5.5f;
const float FLOP_SPEED = 0.6f;
const float KEY_SPEED = 16.0f;
const float LAMP_SPEED = 42.0f;
const float SCENE_LAMP_MAX_MOVE[3] = { 7.0f, 0.0f, 3.0f };
const int WHITE_KEYS_NUMBER = 36;
const int BLACK_KEYS_NUMBER = 25;
const int SCENE_LIGHTS_NUMBER = 3;

class Actions {
private:
    float piano_flop_angle_percentage;
    float key_white_push_percentage[WHITE_KEYS_NUMBER] = {};
    float key_black_push_percentage[BLACK_KEYS_NUMBER] = {};
    glm::vec3 light_direction_move[SCENE_LIGHTS_NUMBER];
    float light_direction_rotate[SCENE_LIGHTS_NUMBER];
    float delta_time_sum = 0.0f;
    float add_to_sin = 180.0f / (SCENE_LIGHTS_NUMBER - 1);

    void check_angles();
    void check_piano_flop_angle();
    void check_piano_key_push_angles();
    bool is_valid_keys_number(int number, bool isWhite);

public:
    Actions(float flop_open_percentage);

    void ProcessKeyboard(key_action KEY, float deltaTime);
    void ProcessKeyboard(key_action KEY, float deltaTime, int key, bool isWhite);

    void move_the_lamps(float deltaTime);

    glm::vec3 get_light_direction_move(int lamp);
    float get_light_direction_angle(int lamp);

    float get_flop_angle();
    float get_stick_angle();
    float get_piano_key_angle(int key, bool isWhite);
};

Actions::Actions(float flop_open_percentage) : piano_flop_angle_percentage(flop_open_percentage) {
    for (int i = 0; i < SCENE_LIGHTS_NUMBER; ++i) {
        light_direction_move[i] = glm::vec3(0.0f);
    }
    check_angles();
}

void Actions::ProcessKeyboard(key_action KEY, float deltaTime) {
    float velocity = FLOP_SPEED * deltaTime;
    if (KEY == PIANO_OPEN) {
        piano_flop_angle_percentage += velocity;
    }
    else if (KEY == PIANO_CLOSE) {
        piano_flop_angle_percentage -= velocity;
    }
    check_piano_flop_angle();
}

void Actions::ProcessKeyboard(key_action KEY, float deltaTime, int key, bool isWhite) {
    float velocity = KEY_SPEED * deltaTime;
    if (KEY == PUSH_KEY) {
        if (isWhite) key_white_push_percentage[key] += velocity;
        else key_black_push_percentage[key] += velocity;
    }
    else if (KEY == RELEASE_KEY) {
        if (isWhite) key_white_push_percentage[key] -= velocity;
        else key_black_push_percentage[key] -= velocity;
    }
    check_piano_key_push_angles();
}

void Actions::move_the_lamps(float deltaTime) {
    delta_time_sum += deltaTime * LAMP_SPEED;
    if (delta_time_sum > 360.0f) delta_time_sum -= 360.0f;
    for (int i = 0; i < SCENE_LIGHTS_NUMBER; ++i) {
        light_direction_rotate[i] = delta_time_sum + add_to_sin * i;
        light_direction_move[i].x = SCENE_LAMP_MAX_MOVE[0] * glm::sin(glm::radians(light_direction_rotate[i]));
    }
}

glm::vec3 Actions::get_light_direction_move(int lamp) {
    return light_direction_move[lamp];
}

float Actions::get_light_direction_angle(int lamp) {
    return light_direction_rotate[lamp];
}

float Actions::get_flop_angle() {
    return piano_flop_angle_percentage * PIANO_FLOP_OPEN_ANGLE_MAX;
}

float Actions::get_stick_angle() {
    return (piano_flop_angle_percentage * piano_flop_angle_percentage) * PIANO_STICK_OPEN_ANGLE_MAX;
}

float Actions::get_piano_key_angle(int key, bool isWhite) {
    return (isWhite ? key_white_push_percentage[key] : key_black_push_percentage[key]) * PIANO_KEY_PUSH_ANGLE_MAX;
}

bool Actions::is_valid_keys_number(int number, bool isWhite) {
    return (isWhite && number < WHITE_KEYS_NUMBER) || (!isWhite && number < BLACK_KEYS_NUMBER) || number >= 0;
}

void Actions::check_angles() {
    check_piano_flop_angle();
    check_piano_key_push_angles();
}

void Actions::check_piano_flop_angle() {
    if (piano_flop_angle_percentage < 0.0f) {
        piano_flop_angle_percentage = 0.0f;
    }
    else if (piano_flop_angle_percentage > 1.0f) {
        piano_flop_angle_percentage = 1.0f;
    }
}

void Actions::check_piano_key_push_angles() {
    for (int i = 0; i < WHITE_KEYS_NUMBER; ++i) {
        if (key_white_push_percentage[i] < 0.0f) {
            key_white_push_percentage[i] = 0.0f;
        }
        else if (key_white_push_percentage[i] > 1.0f) {
            key_white_push_percentage[i] = 1.0f;
        }
    }

    for (int i = 0; i < BLACK_KEYS_NUMBER; ++i) {
        if (key_black_push_percentage[i] < 0.0f) {
            key_black_push_percentage[i] = 0.0f;
        }
        else if (key_black_push_percentage[i] > 1.0f) {
            key_black_push_percentage[i] = 1.0f;
        }
    }
}
